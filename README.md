# Usage

 -a, --accesslog <arg>   print the version information and exit

 -d, --duration <arg>    print project help information

 -k, --keeplogs          print the version information and exit

 -s, --startDate <arg>   print this message

 -t, --threshold <arg>   print the version information and exit

# Example

java -cp "parser.jar" com.ef.Parser --accesslog=/path/to/access/log --startDate=2017-01-01.13:00:00 --duration=hourly --threshold=100

# Configuration

Database default connection string:

jdbc:mysql://localhost/efparser?user=root&password=&useSSL=false

database jdbc connection string can be changed with -Djdjdbc.connection.string=<connection_string>   parameter

example:

java java -cp "parser.jar" -Djdbc.connection.string="jdbc:mysql://localhost/efp2?user=root&password=&useSSL=false"  com.ef.Parser --accesslog=/path/to/access/log --startDate=2017-01-01.13:00:00 --duration=hourly --threshold=100


Locations: ( all locations starts from zip file root )

application jar file location: /parser.jar

database schema location: /db_schema.sql

queries location: /queries.sql

Database default config:
username: root
password is empty

