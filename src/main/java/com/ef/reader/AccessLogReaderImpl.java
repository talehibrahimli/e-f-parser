package com.ef.reader;

import com.ef.data.AccessLog;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Iterator;

/**
 * Created by Taleh Ibrahimli on 11/11/17.
 * Email: me@talehibrahimli.com
 */
public class AccessLogReaderImpl implements AccessLogReader {

    private final FileInputStream fis;
    private final BufferedReader reader;

    public AccessLogReaderImpl(File accessLogFile) throws FileNotFoundException {
        this.fis = new FileInputStream(accessLogFile);
        reader = new BufferedReader(new InputStreamReader(fis));
    }

    @Override
    public void close() throws Exception {
        reader.close();
        fis.close();
    }

    @Override
    public Iterable<AccessLog> getAccessLogIterator() {
        return AccessLogReaderIterator::new;
    }

    class AccessLogReaderIterator implements Iterator<AccessLog> {
        private String line;

        @Override
        public boolean hasNext() {
            tryToReadLine();
            return line != null;
        }

        @Override
        public AccessLog next() {
            tryToReadLine();
            AccessLog accessLog = parseAccessLogLine(line);
            line = null;
            return accessLog;
        }

        private void tryToReadLine() {
            if (line == null) {
                try {
                    line = reader.readLine();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        private AccessLog parseAccessLogLine(String line) {
            AccessLog accessLog = new AccessLog();
            try {
                String[] cells = line.split("\\|");
                if (cells.length != 5) {
                    System.out.println("wrong cell count in line: " + line);
                    return null;
                }
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                accessLog.setDate(sdf.parse(cells[0]));
                accessLog.setAddress(cells[1]);
                accessLog.setRequest(cells[2]);
                accessLog.setResponseStatus(cells[3]);
                accessLog.setUserAgent(cells[4]);
            } catch (ParseException e) {
                System.out.println(e.getMessage() + ": " + line);
                return null;
            }
            return accessLog;
        }
    }
}
