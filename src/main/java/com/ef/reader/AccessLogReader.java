package com.ef.reader;

import com.ef.data.AccessLog;

/**
 * Created by Taleh Ibrahimli on 11/11/17.
 * Email: me@talehibrahimli.com
 */
public interface AccessLogReader extends AutoCloseable {

    Iterable<AccessLog> getAccessLogIterator();

}
