package com.ef.parser;

import com.ef.data.AccessLog;
import com.ef.data.AccessLogThreshold;
import com.ef.data.BlockReason;
import com.ef.data.Duration;
import com.ef.data.db.Dao;
import com.ef.data.db.JdbcDaoImpl;
import com.ef.reader.AccessLogReader;
import com.ef.reader.AccessLogReaderImpl;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

// Parser runner class
public class ParserRunner {

    private Dao dao;

    // Parser run method which is responsible to read arguments and start operation
    public void run(String startDateStr, String durationStr, String thresholdStr, String accessLogStr, boolean keepLogs) throws ParseException, FileNotFoundException {
        // create new instance of dao and open it
        try {
            dao = new JdbcDaoImpl();
            dao.open();

            // if accessLog file property exists read and load it to database
            if (!org.apache.commons.lang3.StringUtils.isEmpty(accessLogStr)) {
                this.importAccessLogs(accessLogStr, keepLogs);
            }

            // parsing arguments
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd.HH:mm:ss");
            Date startDate = sdf.parse(startDateStr);
            Duration duration = Duration.valueOf(durationStr.toUpperCase());
            int threshold = Integer.parseInt(thresholdStr);

            // find blocked addresses corresponding to specified parameters
            this.findBlockedAddresses(startDate, duration, threshold);
        } finally {
            // close database connection
            try {
                if (dao != null) {
                    dao.close();
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private void findBlockedAddresses(Date startDate, Duration duration, int threshold) {
        List<AccessLogThreshold> blockedAddresses = dao.findAccessLogsWithThreshold(startDate, duration, threshold);
        for (AccessLogThreshold accessLogThreshold : blockedAddresses) {
            BlockReason blockReasonMaker = new BlockReason(accessLogThreshold, startDate, duration, threshold);
            System.out.println(blockReasonMaker.getComment()); // change to logged
            dao.blockAddressWithReason(blockReasonMaker);
        }
    }

    private void importAccessLogs(String accessLogStr, boolean keepLogs) {
        Date t1 = new Date();
        if (!keepLogs) {
            dao.cleanAccessLogs();
        }
        File accessLogFile = new File(accessLogStr);
        // create new instance and open AccessLogReader
        try (AccessLogReader accessLogReader = new AccessLogReaderImpl(accessLogFile)) {
            // get access logs with iteration
            for (AccessLog accessLog : accessLogReader.getAccessLogIterator()) {
                // add access log to database
                if (accessLog == null) continue;
                dao.addAccessLog(accessLog);
            }
            dao.flush();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        Date t2 = new Date();
        System.out.println(t2.getTime() - t1.getTime());
    }
}
