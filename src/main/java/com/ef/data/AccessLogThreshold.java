package com.ef.data;

import java.sql.Date;

/**
 * Created by Taleh Ibrahimli on 11/11/17.
 * Email: me@talehibrahimli.com
 */
public class AccessLogThreshold {
    private String address;
    private int count;
    private Date lastDate;

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public void setLastDate(Date lastDate) {
        this.lastDate = lastDate;
    }

    public Date getLastDate() {
        return lastDate;
    }
}
