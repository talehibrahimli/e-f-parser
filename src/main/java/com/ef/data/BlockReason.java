package com.ef.data;

import java.util.Date;

/**
 * Created by Taleh Ibrahimli on 11/11/17.
 * Email: me@talehibrahimli.com
 */
public class BlockReason {

    private final String COMMENT_FORMAT = "%s address has %d request between %s and %s which is more than allowed %d request";

    private final AccessLogThreshold accessLogThreshold;
    private final Date startDate;
    private final Duration duration;
    private final int threshold;

    public BlockReason(AccessLogThreshold accessLogThreshold, Date startDate, Duration duration, int threshold) {

        this.accessLogThreshold = accessLogThreshold;
        this.startDate = startDate;
        this.duration = duration;
        this.threshold = threshold;
    }

    public String getAddress() {
        return this.accessLogThreshold.getAddress();
    }

    public Date getLastRequestDate() {
        return accessLogThreshold.getLastDate();
    }

    public String getComment() {
        return String.format(COMMENT_FORMAT, getAddress(), accessLogThreshold.getCount(), startDate, Duration.calcEndDate(startDate, duration), threshold);
    }
}
