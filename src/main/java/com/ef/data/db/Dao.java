package com.ef.data.db;

import com.ef.data.AccessLog;
import com.ef.data.AccessLogThreshold;
import com.ef.data.BlockReason;
import com.ef.data.Duration;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * Created by Taleh Ibrahimli on 11/11/17.
 * Email: me@talehibrahimli.com
 */
public interface Dao {
    void open();

    void cleanAccessLogs();

    void addAccessLog(AccessLog accessLog);

    List<AccessLogThreshold> findAccessLogsWithThreshold(Date startDate, Duration duration, int threshold);

    void close() throws SQLException;

    void blockAddressWithReason(BlockReason blockReasonMaker);

    void flush() throws SQLException;
}
