package com.ef.data.db;

import com.ef.data.AccessLog;
import com.ef.data.AccessLogThreshold;
import com.ef.data.BlockReason;
import com.ef.data.Duration;
import org.apache.commons.lang3.StringUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Taleh Ibrahimli on 11/11/17.
 * Email: me@talehibrahimli.com
 */
public class JdbcDaoImpl implements Dao {

    private static final String DEFAULT_CONNECTION_STRING = "jdbc:mysql://localhost/efparser?user=root&password=&useSSL=false";
    private static final String CLEAN_ACCESS_LOGS_STATEMENT = "DELETE FROM access_log";
    private static final String ADD_ACCESS_LOGS_STATEMENT = "INSERT INTO access_log (date, address, request, response_status, user_agent) values(?, ?, ?, ?, ?)";
    private static final String FIND_ACCESS_LOGS_COUNT_STATEMENT = "SELECT address , count(*) as count , max(date) as last_date FROM access_log WHERE date >= ? AND date <= ? GROUP BY 1 HAVING count(*) > ?";
    private static final String ADD_BLOCKED_ADDRESS_STATEMENT = "INSERT INTO blocked_address (address, block_date, last_request_date, comment) values(?, ?, ?, ?)";
    private static final int INSERT_ACCESS_LOG_BATCH_SIZE = 100;

    private Connection conn;

    private PreparedStatement accessLogPreparedStatement;

    private String getConnectionString() {
        String connectionString = System.getProperty("jdbc.connection.string");
        if (!StringUtils.isEmpty(connectionString)) {
            return connectionString;
        } else {
            return DEFAULT_CONNECTION_STRING;
        }
    }

    @Override
    public void open() {
        try {
            this.conn = DriverManager.getConnection(getConnectionString());
            this.conn.setAutoCommit(false);
            this.accessLogPreparedStatement = this.conn.prepareStatement(ADD_ACCESS_LOGS_STATEMENT);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void cleanAccessLogs() {
        try {
            PreparedStatement pstm = this.conn.prepareStatement(CLEAN_ACCESS_LOGS_STATEMENT);
            pstm.execute();
            pstm.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private int queuedAccessLogCount = 0;

    @Override
    public void addAccessLog(AccessLog accessLog) {
        try {
            accessLogPreparedStatement.setDate(1, new java.sql.Date(accessLog.getDate().getTime()));
            accessLogPreparedStatement.setString(2, accessLog.getAddress());
            accessLogPreparedStatement.setString(3, accessLog.getRequest());
            accessLogPreparedStatement.setString(4, accessLog.getResponseStatus());
            accessLogPreparedStatement.setString(5, accessLog.getUserAgent());
            accessLogPreparedStatement.addBatch();
            this.queuedAccessLogCount++;
            if (this.queuedAccessLogCount >= INSERT_ACCESS_LOG_BATCH_SIZE) {
                flush();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void flush() throws SQLException {
        if (this.queuedAccessLogCount > 0) {
            this.accessLogPreparedStatement.executeBatch();
            this.conn.commit();
            this.queuedAccessLogCount = 0;
        }
    }

    @Override
    public List<AccessLogThreshold> findAccessLogsWithThreshold(java.util.Date startDate, Duration duration, int threshold) {
        List<AccessLogThreshold> list = new ArrayList<>();
        PreparedStatement pstm = null;
        ResultSet rs = null;
        try {
            pstm = this.conn.prepareStatement(FIND_ACCESS_LOGS_COUNT_STATEMENT);
            pstm.setDate(1, new java.sql.Date(startDate.getTime()));
            pstm.setDate(2, new java.sql.Date(Duration.calcEndDate(startDate, duration).getTime()));
            pstm.setInt(3, threshold);
            rs = pstm.executeQuery();
            while (rs.next()) {
                AccessLogThreshold accessLogThreshold = new AccessLogThreshold();
                accessLogThreshold.setAddress(rs.getString(1));
                accessLogThreshold.setCount(rs.getInt(2));
                accessLogThreshold.setLastDate(rs.getDate(3));
                list.add(accessLogThreshold);
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (pstm != null) {
                    pstm.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return Collections.emptyList();
    }

    @Override
    public void close() throws SQLException {
        if (this.accessLogPreparedStatement != null) {
            this.accessLogPreparedStatement.close();
        }
        if (this.conn != null) {
            this.conn.close();
        }
    }

    @Override
    public void blockAddressWithReason(BlockReason blockReason) {
        try {
            PreparedStatement pstm = this.conn.prepareStatement(ADD_BLOCKED_ADDRESS_STATEMENT);
            pstm.setString(1, blockReason.getAddress());
            pstm.setDate(2, new Date(new java.util.Date().getTime()));
            pstm.setDate(3, new Date(blockReason.getLastRequestDate().getTime()));
            pstm.setString(4, blockReason.getComment());
            pstm.execute();
            pstm.close();
            conn.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
