package com.ef.data;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Taleh Ibrahimli on 11/11/17.
 * Email: me@talehibrahimli.com
 */
public enum Duration {
    HOURLY, DAILY;

    public static Date calcEndDate(Date startDate, Duration duration) {
        Calendar endDateCalendar = Calendar.getInstance();
        endDateCalendar.setTime(startDate);
        switch (duration) {
            case DAILY:
                endDateCalendar.add(Calendar.DATE, 1);
                break;
            case HOURLY:
                endDateCalendar.add(Calendar.HOUR, 1);
                break;
        }
        return endDateCalendar.getTime();
    }
}
