package com.ef.data;

import java.util.Date;

/**
 * Created by Taleh Ibrahimli on 11/11/17.
 * Email: me@talehibrahimli.com
 */
public class AccessLog {
    private Date date;
    private String address;
    private String request;
    private String responseStatus;
    private String userAgent;

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getRequest() {
        return request;
    }

    public void setResponseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
    }

    public String getResponseStatus() {
        return responseStatus;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getUserAgent() {
        return userAgent;
    }

    @Override
    public String toString() {
        return String.format("%s\t%s\t%s\t%s\t%s\t", date, address, request, responseStatus, userAgent);
    }
}
