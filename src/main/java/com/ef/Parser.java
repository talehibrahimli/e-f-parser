package com.ef;

import com.ef.parser.ParserRunner;
import org.apache.commons.cli.*;

import java.io.FileNotFoundException;

// Parser class with main entry point
public class Parser {

    //main entry point for parser class
    public static void main(String... args) {
        Options options = new Options();

        // definition of options which is used by parser
        Option startDateOption = new Option("s", "startDate", true, "start date parameter (required)");
        Option durationOption = new Option("d", "duration", true, "duration parameter: hourly, daily (required)");
        Option thresholdOption = new Option("t", "threshold", true, "threshold (required)");
        Option accessLogOption = new Option("a", "accesslog", true, "access log file location");
        Option keepPreviousLogs = new Option("k", "keeplogs", false, "dont delete previous logs from database");

        // setting all options as required
        accessLogOption.setRequired(false); // if access log file does not set, it will check only data from database
        keepPreviousLogs.setRequired(false); // if access log file does not set, it will check only data from database
        startDateOption.setRequired(true);
        durationOption.setRequired(true);
        thresholdOption.setRequired(true);

        // adding all options to option list
        options.addOption(accessLogOption);
        options.addOption(keepPreviousLogs);
        options.addOption(startDateOption);
        options.addOption(durationOption);
        options.addOption(thresholdOption);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();

        // try to parser cli arguments. if something is getting wrong show errors and usage
        try {
            // parse the command line arguments
            CommandLine line = parser.parse(options, args);
            // copy all parsed arguments to variables
            String startDate = line.getOptionValue(startDateOption.getOpt());
            String duration = line.getOptionValue(durationOption.getOpt());
            String threshold = line.getOptionValue(thresholdOption.getOpt());
            String accessLog = line.getOptionValue(accessLogOption.getOpt());
            boolean keepLogs = line.hasOption(keepPreviousLogs.getOpt());

            // define parser runner and send arguments to it
            ParserRunner parserRunner = new ParserRunner();
            parserRunner.run(startDate, duration, threshold, accessLog, keepLogs);
        } catch (ParseException | java.text.ParseException exp) {
            // show errors
            System.err.println(exp.getMessage());
            // show usage
            formatter.printHelp("parser", options);
        } catch (FileNotFoundException e) {
            // show error about file not found
            System.err.println("access log file not found");
            // show usage
            formatter.printHelp("parser", options);
        }
    }
}
